const axios = require("axios")
const fns = require("date-fns")
const beep = require("beepbeep")

const appointmentProfilesEP = "https://pass-og-id.politiet.no/qmaticwebbooking/rest/schedule/appointmentProfiles/"
const availableDatesEP = "https://pass-og-id.politiet.no/qmaticwebbooking/rest/schedule/branches"
const wanted: string[] = [
  "Bærum politistasjon",
  "Drammen politistasjon",
  "Gran politistasjon",
  "Hamar politistasjon",
  "Grønland politistasjon",
  "Hønefoss politistasjon",
  "Kongsvinger politistasjon",
  "Elverum politistasjon",
  "Fagernes politistasjon",
  "Gjøvik politistasjon",
  "Grenland politistasjon",
  "Hamar politistasjon",
  "Kongsberg politistasjon",
  "Lillehammer politistasjon",
  "Notodden politistasjon",
  "Politiets publikumssenter - Grålum",
  "Politiets publikumssenter - Lillestrøm",
  "Politiets publikumssenter - Ski",
  "Politiets publikumssenter i Stokke",
]
const wantedMonths = [2, 3, 4] // March, April, May, June since Date-FNS getMonth() for January is 0, Feb is 1 etc...
const fetchTimeout = 120000
const goRatelimitUndetected = async (policeStation: Branch["branchName"]) =>
  await new Promise((resolve) => {
    const wait = Math.random() * 200
    console.log("fetch dates for " + policeStation + " in " + wait + " ms")
    setTimeout(resolve, wait)
  })

type Branches = Array<Branch>

type Branch = {
  branchName: string
  branchPublicId: string
  serviceGroups:
    | Array<{
        services: Array<{
          additionalCustomerDuration: number
          custom: string
          duration: number
          name: "Kun Pass" | "Både pass og ID-kort til samme person" | "Kun ID-kort"
          publicId: string
        }>
      }>
    | undefined
} & {
  servicePublicId: string
}

type AvailableDatesForLocation = {
  [location: Branch["branchName"]]: Array<string>
}

type DateEntry = {
  date: Date
}

const getWantedLocations = async (): Promise<Branches> => {
  const response = await axios.get(appointmentProfilesEP)
  const branches = response.data as Branches
  const filteredBranches = branches.filter((branch) => {
    return (
      wanted.includes(branch.branchName) &&
      branch.serviceGroups &&
      branch.serviceGroups.some((serviceGroup) => {
        const hit = serviceGroup.services.find((service) => service.name === "Kun Pass")
        if (hit) {
          branch.servicePublicId = hit.publicId
          delete branch.serviceGroups
        }
        return hit
      })
    )
  })
  return filteredBranches
}

const getAvailableDates = async (): Promise<void> => {
  console.log(" ")
  console.log(" ")
  console.log("*************")
  console.log("** CONFIG: **")
  console.log("*************")
  console.log(" ")
  console.log("Refetch Timeout: " + fetchTimeout / 1000 + "sec")
  console.log("Wanted Months: " + wantedMonths.map((m) => m + 1).join(", "))
  console.log("Wanted Police Stations:")
  console.log(wanted.map((w) => "   - " + w).join("\n"))

  console.log(" ")
  console.log("***************************************")
  console.log("**  RESULTS: (refetching in " + fetchTimeout / 1000 + " sec) **")
  console.log("***************************************")
  console.log(" ")

  const wantedLocations = await getWantedLocations()
  const availableDatesForLocation: AvailableDatesForLocation = {}
  for (const wantedLocation of wantedLocations) {
    await goRatelimitUndetected(wantedLocation.branchName)
    const response = await axios.get(`${availableDatesEP}/${wantedLocation.branchPublicId}/dates;servicePublicId=${wantedLocation.servicePublicId};customSlotLength=20`)
    const availableDates = response.data as DateEntry[]
    if (availableDates.length > 0) {
      const dates = availableDates
        .map((availableDate) => availableDate.date)
        .filter((date) => {
          const dt = fns.parseISO(date)
          return wantedMonths.includes(dt.getMonth())
        })
        .map((filtered) => {
          const dt = fns.parseISO(filtered)
          return fns.format(dt, "d MMM")
        })
      if (dates.length > 0) availableDatesForLocation[wantedLocation.branchName] = dates
    }
  }
  console.log(availableDatesForLocation)
  if (Object.keys(availableDatesForLocation).length > 0) {
    beep()
  }
}

function loop() {
  getAvailableDates()
  const intervalID = setInterval(getAvailableDates, fetchTimeout)
}

loop()
